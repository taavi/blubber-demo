#!/bin/bash
set -x
buildctl \
    --addr=podman-container://buildkitd \
    build \
            --frontend=gateway.v0 \
            --opt source=docker-registry.wikimedia.org/repos/releng/blubber/buildkit:v0.16.0 \
            --local context=. \
            --local dockerfile=. \
            --opt "filename=${1?Missing the path to the blubber file}" \
            --opt target=${2?Missing the blubber variant to build} \
            --output "type=docker,name=${3?Missing the destination image name}" \
| podman load