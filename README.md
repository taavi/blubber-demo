# Blubber WMCS demo

This is a simple demo of how to build images with blubber, focused on simple
examples rather than exhaustive ones.

## Intro

### What is blubber

Blubber generates dockerfiles from yaml files, using best practices and allowing
simpler code.

It does things like:

- Avoid using root user
- Update and later clean caches for apt when installing packages

blubberfile.yaml -> <blubber> -> DOCKERFILE

### What is buildkit

Buildkit is a docker project, to enhance the way they build container images.
The feature we use is that it allows to use a custom command (specified as a
container image) to parse the input files and generate dockerfiles that it will
use to build the images.

customfile --> <custom script> -DOCKERFILE-> buildkit --> container image

### docker buildx

This is a plugin for docker itself, that includes an embedded buildkit client.
It replaces the regular `docker build` command (aliases it to
`docker buildx build`).

It makes it transparent to run buildkit based builds:

```
docker build -f path/to/blubber.yaml
```

It also extends the docker server to have a bulitin buildkit builder, so you
don't have to care about starting a buildkitd daemon yourself.

### podman and buildctl

Podman has no such plugin, so you can't use podman directly (afaik) to run
buildkit builds.

Instead you have to use the buildkit client, buildctl, and load the image into
docker.

Note that you can also us this method with docker and not need `buildx`, though
`buildx` does most of the setup for you.

This is also the way that CI builds images.

You will also need to start a buildkitd daemon, you can easily do so like:

```
podman run --rm -d --name buildkitd --privileged moby/buildkit:latest
```

Then you'll need a long command to run a build, you can use `build_podman.sh`
script instead (feel free to look to see the whole command).

## Playing with blubber

### hello_world.yaml

First example

### silly_python_v1.yaml

Installing packages

### silly_python_v2.yaml

Coping files into the images

### silly_python_v3.yaml

Users and permissions

# See also

- https://wikitech.wikimedia.org/wiki/Blubber/User_Guide
